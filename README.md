# Header 1
Hello World!

# Answers

## Answers to class 6's learning unit 1
### Exercise 1

Quantopian is a company that aims to provide the tools needed so that everyone can write an investment strategy. The zipline project is the tool created to do so, it is a comprehensive python package that can be used for backtesting and live-trading.

### Exercise 2

The set slippage function estimates the impact of the order size on the execution price. It also imposes restrictions on the order size according to the market's volume.

### Exercise 3

The function handle_data, in this example, is creating a new order for each stock if a new order has yet to be created. The strategy is to buy 100 shares of Apple and Microsoft and hold them forever.

### Exercise 4
The _test_args function is not part of the zipline framework. It is a function that uses the pandas library to create the start and end date of the backtest that will be used as an argument in the zipline's run_algorithm function.
Although the function datetime from the package datetime can also be used.

Test, don´t mind me